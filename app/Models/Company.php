<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = "companies";

    protected $fillable = [
        'id',
        'trading_name',
        'fantasy_name',
        'cnpj',
        'id_accountable'
    ];

    public function accountable()
    {
        return $this->hasOne('App\Models\Accountable', 'id', 'id_accountable');
    }
}
