<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Accountable;
use Illuminate\Http\Request;

class AccountableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accountable = Accountable::get();
        return response()->json($accountable, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'cpf' => 'required|string',
            'email' => 'required|string',
        ]);

        $accountable = new Accountable();
        $accountable->name = $request->input('name');
        $accountable->cpf = $request->input('cpf');
        $accountable->email = $request->input('email');

        $accountable->save();

        return response()->json($accountable, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $accountable = Accountable::where("id", $id)->first();
        return response()->json($accountable, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'string',
            'cpf' => 'string',
            'email' => 'string',
        ]);

        $accountableUpdated = $request->only([
            'name', 'cpf', 'email'
        ]);

        $accountable = Accountable::where('id', $id)->first();

        $accountable->update($accountableUpdated);
        $accountable->save();

        return response()->json($accountable, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $accountable = Accountable::where('id', $id)->first();
        $accountable->delete();
 
        return response()->json($accountable, 200);
    }
}
