<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = Company::with(['accountable'])->get();
        return response()->json($company, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'trading_name' => 'required|string',
            'fantasy_name' => 'required|string',
            'cnpj' => 'required|string',
            'id_accountable' => 'nullable|numeric',
        ]);

        $company = new Company();
        $company->trading_name = $request->input('trading_name');
        $company->fantasy_name = $request->input('fantasy_name');
        $company->cnpj = $request->input('cnpj');
        $company->id_accountable = $request->input('id_accountable');

        $company->save();

        return response()->json($company, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::with(['accountable'])->where("id", $id)->first();
        return response()->json($company, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'trading_name' => 'string',
            'fantasy_name' => 'string',
            'cnpj' => 'string',
            'id_accountable' => 'nullable|numeric',
        ]);

        $companyUpdated = $request->only([
            'trading_name', 'fantasy_name', 'cnpj', 'id_accountable'
        ]);

        $company = Company::where('id', $id)
            ->with(['accountable'])->first();

        $company->update($companyUpdated);
        $company->save();

        return response()->json($company, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::where('id', $id)->first();
        $company->delete();

        return response()->json($company, 200);
    }
}
