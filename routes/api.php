<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'api', 'middleware' => ['api']], function () {

    //CompanyController
    Route::get("/company", "CompanyController@index");
    Route::post("/company", "CompanyController@store");
    Route::get("/company/{id}", "CompanyController@show");
    Route::put("/company/{id}", "CompanyController@update");
    Route::delete("/company/{id}", "CompanyController@destroy");

    //AccountableController
    Route::get("/accountable", "AccountableController@index");
    Route::post("/accountable", "AccountableController@store");
    Route::get("/accountable/{id}", "AccountableController@show");
    Route::put("/accountable/{id}", "AccountableController@update");
    Route::delete("/accountable/{id}", "AccountableController@destroy");
});
