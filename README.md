# Laravel PHP Framework - CRUD


## O que é este projeto?
O projeto tem como objetivo criar uma api para a realizar um CRUD para a empresa e seu resposável.


## Pré-requisitos
- PHP >= 7
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension


## Para rodar este projeto
```bash
$ git clone https://CamilaASales@bitbucket.org/camilaasales/management-api.git
$ cd management-api
$ composer install
$ cp .env.example .env
$ php artisan key:generate
$ php artisan migrate #antes de rodar este comando verifique sua configuracao com banco em .env
$ php artisan serve
```
Acesssar pela url: http://localhost:8080


